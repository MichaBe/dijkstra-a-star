#include "pch.h"
#include "Knoten.h"


Knoten::Knoten()
{
}

Knoten::Knoten(std::string name, float x, float y, float z, float heuristik)
{
	m_Name = name;
	m_Heuristik = heuristik;
	m_Coordinates[0] = x;
	m_Coordinates[1] = y;
	m_Coordinates[2] = z;
}


Knoten::~Knoten()
{
}

float Knoten::GetDistance(Knoten & target)
{
	float distance = 0.0f;
	for (int i = 0; i < 3; ++i) {
		distance += (target.m_Coordinates[i] - m_Coordinates[i])*(target.m_Coordinates[i] - m_Coordinates[i]);
	}
	distance = distance <= 0.0f ? 0.0f : std::sqrtf(distance);
	return distance;
}

void Knoten::SetHeuristik(float heuristik)
{
	m_Heuristik = heuristik;
}

std::string Knoten::GetName()
{
	return m_Name;
}

float Knoten::GetHeuristik()
{
	return m_Heuristik;
}
