#include "pch.h"
#include "ListData.h"


ListData::ListData(Knoten * knoten, float cost, Knoten * previous)
{
	m_Knoten = knoten;
	m_Cost = cost;
	m_Previous = previous;
}

ListData::~ListData()
{
}

Knoten * ListData::Get() const
{
	return m_Knoten;
}

float ListData::GetCost() const
{
	return m_Cost;
}

Knoten * ListData::GetPrevious() const
{
	return m_Previous;
}

void ListData::SetNewPrevious(Knoten * previous, float cost)
{
	m_Previous = previous;
	m_Cost = cost;
}

bool ListData::operator==(ListData & other)
{
	return this == &other;
}
