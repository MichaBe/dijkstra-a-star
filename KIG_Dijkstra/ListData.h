#pragma once

#include "Knoten.h"
#include <utility>

class ListData
{
public:
	ListData(Knoten* knoten, float cost, Knoten* previous);
	~ListData();
	Knoten* Get() const;
	float GetCost() const;
	Knoten* GetPrevious() const;
	void SetNewPrevious(Knoten* previous, float cost);
	bool operator==(ListData& other);

private:
	Knoten* m_Knoten;
	float m_Cost;
	Knoten* m_Previous;
};

