#include "pch.h"
#include "Dijkstra.h"


Dijkstra::Dijkstra(Kantenliste * graph, Knoten * start, Knoten * end)
{
	m_Current = new ListData(start,0, nullptr);
	m_Target = end;
	m_Graph = graph;
}

Dijkstra::~Dijkstra()
{
}

bool Dijkstra::Step()
{
	bool returner = true;
	if (m_Current != nullptr) {
		float currentCosts = m_Current->GetCost();
		if (m_Graph->HasNext(m_Current->Get())) {
			auto neighbors = m_Graph->GetNext(m_Current->Get());//alle nachbarn vom aktuellen knoten holen
			for (auto it = neighbors->begin(); it != neighbors->end(); ++it) {
				float newCost = currentCosts + it->first;
				//Schauen, ob schon in Closedlist
				auto it2 = std::find_if(m_ClosedList.begin(), m_ClosedList.end(),
					[it](const ListData i) {return i.Get() == it->second; });
				
				if (it2 != m_ClosedList.end()) {
					//Wenn ja: Kosten evtl. aktualisieren und wieder in openlist verschieben
					if (it2->GetCost() > newCost) {
						//Aktualisieren
						ListData l(m_Current->Get(), newCost, m_Current->GetPrevious());
						m_OpenList.insert(l);
						m_ClosedList.erase(it2);
					}
				}
				else {//Wenn nein: neues pair in Openlist erzeugen
					//Schauen ob schon in OpenList
					auto itOpenList = std::find_if(m_OpenList.begin(), m_OpenList.end(),
						[it](const ListData i) {return i.Get() == it->second; });

					if (itOpenList == m_OpenList.end()) {
						ListData newOpen(it->second, newCost, m_Current->Get());
						m_OpenList.insert(newOpen);
					}
				}
			}
		}
		//current neu setzen
		ListData newL(m_Current->Get(), m_Current->GetCost(), m_Current->GetPrevious());
		m_ClosedList.insert(newL);
		if (m_OpenList.empty())
			m_Current = nullptr;
		else {
			auto i = m_OpenList.begin();
			m_Current = new ListData(i->Get(), i->GetCost(), (i->GetPrevious()));
			m_OpenList.erase(m_OpenList.begin());
		}
	}
	else
		returner = false;
	return returner;
}

void Dijkstra::Out()
{
	if (m_Current != nullptr) {
		std::cout << "Current: " << m_Current->Get()->GetName() << ", Cost: " << m_Current->GetCost();
		if (m_Current->GetPrevious() != nullptr)
			std::cout << ", Prev.: " << m_Current->GetPrevious()->GetName() << std::endl;
		else
			std::cout << std::endl<<std::endl;
	}
	std::cout << "Openlist: \nName, Cost, Prev." << std::endl<< std::endl;
	for (auto it : m_OpenList) {
		std::cout << it.Get()->GetName() << "\t" << it.GetCost() << "\t";
		if (it.GetPrevious() != nullptr)
			std::cout << it.GetPrevious()->GetName() << std::endl;
		else
			std::cout << std::endl;
	}
	std::cout << "=====================================================" << std::endl;
	std::cout << "Closedlist: \nName, Cost, Prev. " << std::endl;
	for (auto it : m_ClosedList) {
		std::cout << it.Get()->GetName() << "\t" << it.GetCost() << "\t";
		if (it.GetPrevious() != nullptr)
			std::cout << it.GetPrevious()->GetName() << std::endl;
		else
			std::cout << std::endl;
	}
}
