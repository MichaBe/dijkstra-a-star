#pragma once

#include <string>
#include <math.h>

class Knoten
{
public:
	Knoten();
	Knoten(std::string name, float x, float y, float z, float heuristik = 0);
	~Knoten();
	float GetDistance(Knoten& target);
	void SetHeuristik(float heuristik);
	std::string GetName();
	float GetHeuristik();

private:
	std::string m_Name;
	float m_Heuristik;
	float m_Coordinates[3];
};

