// KIG_Dijkstra.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include "pch.h"
#include <iostream>
#include <string>
#include "Knoten.h"
#include "Kantenliste.h"
#include "Dijkstra.h"
#include "AStar.h"
using namespace std;

int main()
{
	Knoten A("A", 12, 0, 2);
	Knoten B("B", 5, 0, 5);
	Knoten C("C", 20, 0, 5);
	Knoten D("D", 3, 0, 10);
	Knoten E("E", 10, 0, 10);
	Knoten F("F", 16, 0, 10);
	Knoten G("G", 5, 0, 15);
	Knoten H("H", 13, 0, 15);
	Knoten I("I", 20, 0, 13);
	Knoten J("J", 9, 0, 20);
	Knoten K("K", 20, 0, 20);

	Kantenliste graf;
	graf.AddKante(&A, &B, A.GetDistance(B));
	graf.AddKante(&A, &C, A.GetDistance(C));
	graf.AddKante(&B, &D, B.GetDistance(D));
	graf.AddKante(&B, &E, B.GetDistance(E), false);
	graf.AddKante(&C, &I, C.GetDistance(I));
	graf.AddKante(&C, &E, C.GetDistance(E));
	graf.AddKante(&D, &G, D.GetDistance(G));
	graf.AddKante(&E, &F, E.GetDistance(F));
	graf.AddKante(&F, &H, F.GetDistance(H));
	graf.AddKante(&G, &H, G.GetDistance(H));
	graf.AddKante(&G, &J, G.GetDistance(J));
	graf.AddKante(&H, &I, H.GetDistance(I));
	graf.AddKante(&H, &K, H.GetDistance(K));
	graf.AddKante(&I, &K, I.GetDistance(K));

	cout << "Dijkstra:\t1" << endl;
	cout << "A*: \t\t2" << endl;
	int auswahl = 0;
	cin >> auswahl;

	if (auswahl == 1) {
		Dijkstra d(&graf, &A, &K);
		do {
			d.Out();
			cout << "######################################" << endl << endl;
		} while (d.Step());
	}
	else {
		AStar a(&graf, &A, &K);
		do {
			a.Out();
			cout << "######################################" << endl << endl;
		} while (a.Step());
	}
	int x;
	cin >> x;
}