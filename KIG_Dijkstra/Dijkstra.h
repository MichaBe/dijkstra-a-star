#pragma once

#include "Kantenliste.h"
#include "Knoten.h"
#include "ListData.h"
#include <set>
#include <utility>
#include <algorithm>
#include <functional>
#include <iostream>

class Dijkstra
{
public:
	Dijkstra(Kantenliste* graph, Knoten* start, Knoten* end);
	~Dijkstra();
	bool Step();
	void Out();

private:
	struct PairComperator {
		bool operator()(const ListData &lhs, const ListData &rhs) {
			return lhs.GetCost() < rhs.GetCost();
		}
	};
	ListData* m_Current;
	std::multiset<ListData, PairComperator> m_OpenList;
	std::multiset<ListData, PairComperator> m_ClosedList;
	Kantenliste* m_Graph;
	Knoten* m_Target;

	
};

