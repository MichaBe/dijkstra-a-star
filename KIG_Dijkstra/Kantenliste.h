#pragma once

#include "Knoten.h"
#include <functional>
#include <vector>
#include <map>
#include <utility>

class Kantenliste
{
public:
	Kantenliste();
	~Kantenliste();

	void AddKante(Knoten* von, Knoten* nach, float kosten, bool gerichtet = true);
	void SetHeuristik(std::function<float(Knoten*)> H);
	std::vector<std::pair<float, Knoten*>>* GetNext(Knoten* current);
	bool HasNext(Knoten* current);
	std::map<std::string, Knoten*>* GetKnoten();

private:
	std::map<Knoten*, std::vector<std::pair<float,Knoten*>>> m_Kantenliste;
	std::map<std::string, Knoten*> m_Knotenliste;
};

