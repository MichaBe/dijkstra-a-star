#pragma once

#include "Knoten.h"
#include "Kantenliste.h"
#include "ListData.h"
#include <set>
#include <iostream>
#include <algorithm>

class AStar
{
public:
	AStar(Kantenliste* graph, Knoten* start, Knoten* end);
	~AStar();
	bool Step();
	void Out();

private:
	struct PairComperator {
		bool operator()(const ListData &lhs, const ListData &rhs) {
			return lhs.GetCost()+lhs.Get()->GetHeuristik() < rhs.GetCost()+rhs.Get()->GetHeuristik();
		}
	};
	ListData* m_Current;
	std::multiset<ListData, PairComperator> m_OpenList;
	std::multiset<ListData, PairComperator> m_ClosedList;
	Kantenliste* m_Graph;
	Knoten* m_Target;
};

