#include "pch.h"
#include "Kantenliste.h"


Kantenliste::Kantenliste()
{
}


Kantenliste::~Kantenliste()
{
}

void Kantenliste::AddKante(Knoten* von, Knoten* nach, float kosten, bool gerichtet)
{
	if (m_Knotenliste.count(von->GetName()) == 0)
		m_Knotenliste[von->GetName()] = von;
	if (m_Knotenliste.count(nach->GetName()) == 0)
		m_Knotenliste[nach->GetName()] = nach;

	m_Kantenliste[von].push_back(std::pair<float,Knoten*>(kosten, nach));
	if (!gerichtet)
		m_Kantenliste[nach].push_back(std::pair<float,Knoten*>(kosten, von));
}

void Kantenliste::SetHeuristik(std::function<float(Knoten*)> H)
{
	for (auto it = m_Knotenliste.begin(); it != m_Knotenliste.end(); ++it) {
		it->second->SetHeuristik(H(it->second));
	}
}

std::vector<std::pair<float, Knoten*>>* Kantenliste::GetNext(Knoten* current)
{
	return &(m_Kantenliste.at(current));
}

bool Kantenliste::HasNext(Knoten* current)
{
	return m_Kantenliste.count(current) > 0;
}

std::map<std::string, Knoten*>* Kantenliste::GetKnoten()
{
	return &m_Knotenliste;
}
